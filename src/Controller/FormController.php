<?php
require '../vendor/autoload.php';
//if ($_SERVER['REQUEST_METHOD'] == 'POST'){
//    echo "<h1>SALAMALEKUM</h1>";
//}else{
//    echo "<h1>400 BAD REQUEST</h1>";
//}

use MongoDB\BSON\ObjectId;

// connect to mongodb
$client = new MongoDB\Client('mongodb://localhost:27017');

// select a database
$db = $client->selectDatabase('OnlineShop');

// Select user collection
$userCollection = $db->selectCollection('users');

// Select order collection
$orderCollection = $db->selectCollection('orders');

// Insert a document into the collection
$result = $orderCollection->insertOne([
  'user_id' => new ObjectId("6482e7be45b68299f2deaa7f"),
  'product' => "Product 3",
  'quantity' => 54
]);

// Check if the insertion was successful
if ($result->getInsertedCount() === 1) {
    echo "Document inserted successfully.";
} else {
    echo "Failed to insert the document.";
}

// Find documents in the collection
$documents = $orderCollection->find();

// Iterate through the documents
foreach ($documents as $document) {
    echo $document['product'] . ', ' . $document['quantity'] . "\n";
}

// Update a document in the collection
/*$collection->updateOne(
    ['name' => 'John'],
    ['$set' => ['age' => 31]]
);*/

// Delete a document from the collection
//$collection->deleteOne(['name' => 'John']);
